import { GIAM_SO_LUONG, TANG_SO_LUONG } from "../Constants/numberConstants";

const initialState = {
  number: 0,
};
export const baiTapMiniReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GIAM_SO_LUONG: {
      return {
        ...state,
        number: state.number - payload,
      };
    }
    case TANG_SO_LUONG: {
      return {
        ...state,
        number: state.number + payload,
      };
    }
    default:
      return state;
  }
};
