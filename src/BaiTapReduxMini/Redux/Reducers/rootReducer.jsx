import { combineReducers } from "redux";
import { baiTapMiniReducer } from "./baiTapMiniReducer";

export const rootReducer_baiTapMiniReducer = combineReducers({
  baiTapMiniReducer,
});
