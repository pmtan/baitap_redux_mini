import { GIAM_SO_LUONG, TANG_SO_LUONG } from "../Constants/numberConstants";

export const tangSoLuongAction = (payloadInput) => {
  return {
    type: TANG_SO_LUONG,
    payload: payloadInput,
  };
};

export const giamSoLuongAction = (payloadInput) => {
  return {
    type: GIAM_SO_LUONG,
    payload: payloadInput,
  };
};
