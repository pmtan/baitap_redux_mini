import React, { Component } from "react";
import { connect } from "react-redux";
import {
  giamSoLuongAction,
  tangSoLuongAction,
} from "./Redux/Actions/numberActions";
import {
  GIAM_SO_LUONG,
  TANG_SO_LUONG,
} from "./Redux/Constants/numberConstants";

class BaiTapReduxMini extends Component {
  render() {
    return (
      <div>
        <h1 className="text-dark pt-5">BaiTapReduxMini</h1>
        <h3>{this.props.number}</h3>
        <div>
          <button
            onClick={this.props.giamSoLuong}
            className="btn btn-danger mr-3"
          >
            Giảm Số Lượng
          </button>
          <button onClick={this.props.tangSoLuong} className="btn btn-success">
            Tăng Số Lượng
          </button>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    number: state.baiTapMiniReducer.number,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    tangSoLuong: () => {
      dispatch(tangSoLuongAction(1));
    },
    giamSoLuong: () => {
      dispatch(giamSoLuongAction(1));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(BaiTapReduxMini);
