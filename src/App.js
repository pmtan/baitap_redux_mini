import logo from './logo.svg';
import './App.css';
import BaiTapReduxMini from './BaiTapReduxMini/BaiTapReduxMini';

function App() {
  return (
    <div className="App">
      <BaiTapReduxMini/>
    </div>
  );
}

export default App;
